//Custom time system. Normally, time passes as like in real world
//5s in the real world = 5s in the time system
//Time can be sped up/slowed down by seting transducer functions.

//Using code from John Loane's classes

var TimeSystem = function() {
	this.transducer = function(elapsedTime) {return elapsedTime;};
	this.timer = new AnimationTimer();
	this.lastTimeTransducerWasSet = 0;
	this.gameTime = 0;
};

TimeSystem.prototype = {
	start: function() {
		this.timer.start();
	},

	reset: function() {
		this.timer.stop();
		this.timer.reset();
		this.timer.start();
		this.lastTimeTransducerWasSet = this.gameTime;
	},

	setTransducer: function(transducerFunction, duration) {
		//Duration is optional, indefinite if not set
		var lastTransducer = this.transducer,
			self = this; //Keeping track of current transducer

		this.calculateGameTime();
		this.reset();
		this.transducer = transducerFunction;

		if (duration) {
			setTimeout(function(e) {
				self.setTransducer(lastTransducer);
			}, duration);
		}
	},

	calculateGameTime: function() {
		this.gameTime = this.lastTimeTransducerWasSet + this.transducer(this.timer.getElapsedTime());
		this.reset();
		return this.gameTime;
	}
};