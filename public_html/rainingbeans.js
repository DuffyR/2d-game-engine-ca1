/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var rainingBeans = function() {
    this.screen = document.getElementById("game-screen"),
    this.context = this.screen.getContext("2d"),
    this.fpsElement = document.getElementById("fps"),
    this.toastElement = document.getElementById("toast"),
    this.instructionElement = document.getElementById("instructions"),
    this.copyrightElement = document.getElementById("copyright"),
    this.scoreElement = document.getElementById("score"),
    this.loadingElement = document.getElementById("loading"),
    this.loadingTitleElement = document.getElementById("loading-caption"),
    this.gameOverElement = document.getElementById("game-over"),
    
    //Constants
    this.CANVAS_WIDTH = this.screen.width,
    this.CANVAS_HEIGHT = this.screen.height,
    this.STARTING_POSITION = 200,
    this.STARTING_ALTITUDE = 700,
    this.PLAYER_VELOCITY = 200,
    this.LEFT = -1,
    this.RIGHT = 1,
    this.LEVEL_ONE = 1,
    this.LEVEL_TWO = 2,
    this.LEVEL_THREE = 3,
    this.RESPAWN_POINT = 1200,
    
    //Game States
    this.isMoving = false,
    this.direction = this.RIGHT,
    this.positionX = this.STARTING_POSITION,
    this.positionY = this.STARTING_ALTITUDE,
    this.paused = false,
    this.PAUSED_CHECK_INTERVAL = 200,
    this.pauseStartTime,
    this.windowFocused = true,
    this.countDownInProgress = false,
    this.gameStarted = false,
    this.gameOver = false,
    this.theBeansMoveNow = false,
    this.globalScore = 0,
    this.currentPorgress = this.LEVEL_ONE,
    this.fpsWarning = false,
    
    //Images
    this.background = new Image(),
    //this.player = new Image(),
    this.hazard = new Image(),
    
    //Time
    this.timeSystem = new TimeSystem(),
    this.timeRate = 1.0,
    this.lastAnimationFrameTime = 0,
    this.lastFpsUpdateTime = 0,
    this.fps = 60;
    
    //Keys
    this.KEY_A = 65,
    this.KEY_D = 68,
    this.KEY_LEFT = 37,
    this.KEY_RIGHT = 39,
    this.KEY_P = 80;
    
    //Cell Dimensions
    this.PLAYER_WIDTH = 32,
    this.PLAYER_HEIGHT = this.PLAYER_WIDTH,
    this.BEAN_WIDTH = 48,
    this.BEAN_HEIGHT = this.BEAN_WIDTH,
    this.BOMB_WIDTH = 48,
    this.BOMB_HEIGHT = this.BOMB_WIDTH,
    this.RUBY_HEIGHT = 24, //Width is not constant
    this.EXPLOSION_WIDTH = 48,
    this.EXPLOSION_HEIGHT = this.EXPLOSION_WIDTH;
    
    //Bean Data
    this.beanRedData = [
        {left: 0, top: -50},
        {left: 100, top: -150},
        {left: 300, top: -350},
        {left: 250, top: -375},
        {left: 150, top: -500},
        {left: 350, top: -750},
        {left: 241, top: -850},
        {left: 17, top: -1000},
        {left: 82, top: -1147},
        {left: 11, top: -1180}
    ];
    
    this.beanBlueData = [
        {left: 138, top: -50},
        {left: 50, top: -250},
        {left: 100, top: -450},
        {left: 300, top: -600},
        {left: 153, top: -975},
        {left: 289, top: -1050}
    ];
    
    this.beanGreenData = [
        {left: 200, top: -50},
        {left: 275, top: -600},
        {left: 323, top: -1100},
        {left: 48, top: -2000}
    ];
    
    this.beanYellowData = [
        {left: 200, top: -50},
        {left: 100, top: -200},
        {left: 300, top: -200},
        {left: 34, top: -500},
        {left: 127, top: -600}
    ];
    
    this.beanWhiteData = [
        {left: 300, top: -50},
        {left: 10, top: -200},
        {left: 120, top: -500},
        {left: 234, top: -800},
        {left: 300, top: -1300}
    ];
    
    this.bombData = [
        {left: 294, top: -50, levelSpawn: 1},
        {left: 276, top: -350, levelSpawn: 1},
        {left: 36, top: -600, levelSpawn: 1},
        {left: 266, top: -600, levelSpawn: 1},
        {left: 100, top: -900, levelSpawn: 1},
        {left: 102, top: -1111, levelSpawn: 1},
        {left: 196, top: -100, levelSpawn: 2},
        {left: 54, top: -450, levelSpawn: 2},
        {left: 141, top: -700, levelSpawn: 2},
        {left: 201, top: -900, levelSpawn: 2},
        {left: 101, top: -1200, levelSpawn: 3},
        {left: 310, top: -1681, levelSpawn: 3},
        {left: 22, top: -50, levelSpawn: 3},
        {left: 151, top: -900, levelSpawn: 3},
        {left: 5, top: -600, levelSpawn: 3}
    ];
    
    this.rubyData = [
        {left: 200, top: -500}
    ];
    
    this.playerData = [
        {left: this.positionX, top: this.positionY}
    ];
    
    //Bean Cell
    this.beanRedCell = [
        {left: 0, top: 0, width: this.BEAN_WIDTH, height: this.BEAN_HEIGHT}
    ],
            
    this.beanBlueCell = [
        {left: 48, top: 0, width: this.BEAN_WIDTH, height: this.BEAN_HEIGHT}
    ],
            
    this.beanGreenCell = [
        {left: 96, top: 0, width: this.BEAN_WIDTH, height: this.BEAN_HEIGHT}
    ],
            
    this.beanYellowCell = [
        {left: 144, top: 0, width: this.BEAN_WIDTH, height: this.BEAN_HEIGHT}
    ],
            
    this.beanWhiteCell = [
        {left: 192, top: 0, width: this.BEAN_WIDTH, height: this.BEAN_HEIGHT}
    ],
            
    this.bombCell = [
        {left: 240, top: 0, width: this.BOMB_WIDTH, height: this.BOMB_HEIGHT}
    ],
            
    this.rubyCells = [
        {left: 0, top: 56, width: 18, height: this.RUBY_HEIGHT},
        {left: 24, top: 56, width: 18, height: this.RUBY_HEIGHT},
        {left: 48, top: 56, width: 17, height: this.RUBY_HEIGHT},
        {left: 72, top: 56, width: 16, height: this.RUBY_HEIGHT},
        {left: 96, top: 56, width: 17, height: this.RUBY_HEIGHT},
        {left: 120, top: 56, width: 17, height: this.RUBY_HEIGHT},
        {left: 144, top: 56, width: 17, height: this.RUBY_HEIGHT}
    ];
    
    this.explosionCells = [
        {left: 0, top: 80, width: this.EXPLOSION_WIDTH, height: this.EXPLOSION_HEIGHT},
        {left: 48, top: 80, width: this.EXPLOSION_WIDTH, height: this.EXPLOSION_HEIGHT},
        {left: 96, top: 80, width: this.EXPLOSION_WIDTH, height: this.EXPLOSION_HEIGHT},
        {left: 192, top: 80, width: this.EXPLOSION_WIDTH, height: this.EXPLOSION_HEIGHT},
        {left: 0, top: 128, width: this.EXPLOSION_WIDTH, height: this.EXPLOSION_HEIGHT},
        {left: 48, top: 128, width: this.EXPLOSION_WIDTH, height: this.EXPLOSION_HEIGHT},
        {left: 96, top: 128, width: this.EXPLOSION_WIDTH, height: this.EXPLOSION_HEIGHT},
        {left: 192, top: 128, width: this.EXPLOSION_WIDTH, height: this.EXPLOSION_HEIGHT},
        {left: 0, top: 176, width: this.EXPLOSION_WIDTH, height: this.EXPLOSION_HEIGHT},
        {left: 48, top: 176, width: this.EXPLOSION_WIDTH, height: this.EXPLOSION_HEIGHT},
        {left: 96, top: 176, width: this.EXPLOSION_WIDTH, height: this.EXPLOSION_HEIGHT},
        {left: 192, top: 176, width: this.EXPLOSION_WIDTH, height: this.EXPLOSION_HEIGHT},
        {left: 0, top: 224, width: this.EXPLOSION_WIDTH, height: this.EXPLOSION_HEIGHT},
        {left: 48, top: 224, width: this.EXPLOSION_WIDTH, height: this.EXPLOSION_HEIGHT},
        {left: 96, top: 224, width: this.EXPLOSION_WIDTH, height: this.EXPLOSION_HEIGHT},
        {left: 192, top: 224, width: this.EXPLOSION_WIDTH, height: this.EXPLOSION_HEIGHT}
    ];
    
    this.playerCells = [
        {left: 256, top: 52, width: this.PLAYER_WIDTH, height: this.PLAYER_HEIGHT},
        {left: 256, top: 84, width: this.PLAYER_WIDTH, height: this.PLAYER_HEIGHT}
    ];
            
    this.BEAN_DROP_VELOCITY = 200,
    this.BOMB_DROP_VELCOITY = this.BEAN_DROP_VELOCITY * 1.1,
    this.RUBY_DROP_VELOCITY = this.BEAN_DROP_VELOCITY * 0.75;
            
     //Sprite behaviour
    this.dropBehaviour = { //This makes the sprites fall
        respawn: function(sprite) {
            sprite.top = -100 - (Math.random() * 32);
            sprite.left = Math.floor((Math.random() * (rainBeans.CANVAS_WIDTH - sprite.width)));
            if (sprite.type === "ruby") { //Makes a ruby more rare
                sprite.visible = Math.random() > 0.75 ? true : false;
            } else {
                if (!sprite.visible) {
                    sprite.visible = true;
                }
            }
        },
        
        execute: function(sprite, now, fps, context, lastAnimationFrameTime) {
            if (rainBeans.theBeansMoveNow) {
                var pixelsToMove = sprite.speedY * (now - lastAnimationFrameTime) / 1000;
                if (sprite.top > rainBeans.RESPAWN_POINT) {
                    this.respawn(sprite);
                }
                sprite.top += pixelsToMove;
            }
        }
    };
    
    this.leftRightBehaviour = {//Beans with this behaviour will move left & right, bouncing off walls
        setDirection: function(sprite) {//Turn the bean if it hits the walls
            var rightBoundary = rainBeans.CANVAS_WIDTH - sprite.width;
            
            if (sprite.direction === undefined) {
                sprite.direction = rainBeans.LEFT;
            }
            if (sprite.left < 0 && sprite.direction === rainBeans.LEFT) {
                sprite.direction = rainBeans.RIGHT;
            } else if (sprite.left > rightBoundary && sprite.direction === rainBeans.RIGHT) {
                sprite.direction = rainBeans.LEFT;
            }
        },
        
        setPosition: function(sprite, now, lastAnimationFrameTime) {
            var pixelsToMove = (sprite.speedX * (now - lastAnimationFrameTime) / 1000) * sprite.direction;
            sprite.left += pixelsToMove;
        },
        
        execute: function(sprite, now, fps, context, lastAnimationFrameTime) {
            this.setDirection(sprite);
            this.setPosition(sprite, now, lastAnimationFrameTime);
        }
    };
    
    this.pointsBehaviour = {//Collide with a bean and you will get points
        execute: function(sprite, now, fps, context, lastAnimationFrameTime) {
            var centerX = sprite.left + (sprite.width / 2),
                centerY = sprite.top + (sprite.height / 2);
        
            if (sprite.visible && !rainBeans.gameOver) {
                if (centerX > rainBeans.positionX && centerX < rainBeans.positionX + rainBeans.PLAYER_WIDTH) {
                    if (sprite.top <= rainBeans.positionY && sprite.top + rainBeans.PLAYER_HEIGHT >= rainBeans.positionY) {
                        sprite.visible = false;
                        rainBeans.globalScore += sprite.value;
                    }
                }  
            }
        }
    };
    
    this.bombCollideBehaviour = {//You lose if you collide with a bomb
        viableForCollision: function(sprite) {
            var centerX = sprite.left + (sprite.width / 2),
                leftBound = rainBeans.positionX,
                rightBound = rainBeans.positionX + rainBeans.PLAYER_WIDTH,
                upBound = rainBeans.positionY - 10,
                downBound = rainBeans.positionY + rainBeans.PLAYER_HEIGHT;
        
            return centerX > leftBound && centerX < rightBound && sprite.top >= upBound && sprite.top <= downBound && (sprite.visible === true && sprite.exploding === false);
                
        },
        
        execute: function(sprite, now, fps, context, lastAnimationFrameTime) {
            if (this.viableForCollision(sprite) && !rainBeans.gameOver) {
                rainBeans.gameOver = true;
                rainBeans.player.visible = false;
                sprite.exploding = true;
                //sprite.visible = false;
            }  
        }
    };
    
    this.rubyCollideBehaviour = { //Collect it to destroy all bombs in view
        isABombInView: function(sprite) {
            return sprite.type === "bomb" && (sprite.top > -sprite.height && sprite.top < rainBeans.CANVAS_HEIGHT);
        },
        
        destroyBombs: function() {
            for (var i = 0; i < rainBeans.sprites.length; i++) {
                var sprite = rainBeans.sprites[i];
                if (this.isABombInView(sprite)) {
                    sprite.exploding = true;
                    //sprite.visible = false;
                }
            }
        },
        
        execute: function(sprite, now, fps, context, lastAnimationFrameTime) {
            var centerX = sprite.left + (sprite.width / 2);
            
            if (sprite.visible && !rainBeans.gameOver) {
                if (centerX > rainBeans.positionX && centerX < rainBeans.positionX + rainBeans.PLAYER_WIDTH) {
                    if (sprite.top <= rainBeans.positionY && sprite.top + rainBeans.PLAYER_HEIGHT >= rainBeans.positionY) {
                        this.destroyBombs();
                        sprite.visible = false;
                    }
                }  
            }
        }
    };
    
    this.dropBurstBehaviour = { //Alters the bean's speed
        pause: function(sprite, now) {
            if (sprite.dropTimer.isRunning()) {
                sprite.dropTimer.pause(now);
            }
        },
        
        unpause: function(sprite, now) {
            if (sprite.dropTimer.isRunning()) {
                sprite.dropTimer.unpause(now);
            }
        },
        
        isDropping: function(sprite) {
            return sprite.dropTimer.isRunning();
        },
        
        dropBean: function(sprite, now) {
            var elapsedTime = sprite.dropTimer.getElapsedTime(now),
                deltaY = elapsedTime / (sprite.DROP_DURATION) * sprite.DROP_LENGTH;
        
            sprite.speedY = sprite.startingSpeed - deltaY;
        },
        
        isDoneDropping: function(sprite, now) {
            return sprite.dropTimer.getElapsedTime(now) > sprite.DROP_DURATION;
        },
        
        finishDrop: function(sprite, now) {
            sprite.speedY = sprite.startingSpeed;
            sprite.stopDropping();
        },
        
        execute: function(sprite, now, fps, context, lastAnimationFrameTime) {
            if (!sprite.dropping) {
                return;
            }
            if (this.isDropping(sprite)) {
                if (!this.isDoneDropping(sprite, now)) {
                    this.dropBean(sprite, now);
                } else {
                    this.finishDrop(sprite, now);
                }
            }
        }
    };
    
    this.bombExplosionBehaviour = new CellSwitchBehaviour(this.explosionCells, 500, function(sprite, now, fps){return sprite.exploding;}, function(sprite, animator){ sprite.exploding = false; sprite.visible = false;});
    
    //Sprites
    //this.sprites = [];
    this.beansRed = [];
    this.beansBlue = [];
    this.beansGreen = [];
    this.beansYellow = [];
    this.beansWhite = [];
    this.bombs = [];
    this.rubies = [];
    //this.player = null;
    this.sprites = [];
    
};

rainingBeans.prototype = {
    createSprites: function() {
        this.createRedBeanSprites();
        this.createBlueBeanSprites();
        this.createGreenBeanSprites();
        this.createYellowBeanSprites();
        this.createWhiteBeanSprites();
        this.createBombSprites();
        this.createRubySprites();
        this.createPlayerSprite();
        this.initializeSprites();
        this.addBeansToAllBeansArray();
    },
    
    createRedBeanSprites: function() {
        var bean;
        for (var i = 0; i < this.beanRedData.length; i++) {
            bean = new Sprite("redBean", new SpriteSheetArtist(this.hazard, this.beanRedCell), [this.dropBehaviour, this.pointsBehaviour]);
            bean.width = this.BEAN_WIDTH;
            bean.height = this.BEAN_HEIGHT;
            bean.speedY = this.BEAN_DROP_VELOCITY;
            bean.value = 100;
            this.beansRed.push(bean);
        }
    },
    
    createBlueBeanSprites: function() {
        var bean;
        for (var i = 0; i < this.beanBlueData.length; i++) {
            bean = new Sprite("blueBean", new SpriteSheetArtist(this.hazard, this.beanBlueCell), [this.dropBehaviour, this.leftRightBehaviour, this.pointsBehaviour]);
            bean.width = this.BEAN_WIDTH;
            bean.height = this.BEAN_HEIGHT;
            bean.speedY = 0;
            bean.speedX = 100;
            bean.value = 150;
            this.beansBlue.push(bean);
        }
    },
    
    createGreenBeanSprites: function() {
        var bean;
        for (var i = 0; i < this.beanGreenData.length; i++) {
            bean = new Sprite("greenBean", new SpriteSheetArtist(this.hazard, this.beanGreenCell), [this.dropBehaviour, this.pointsBehaviour]);
            bean.width = this.BEAN_WIDTH;
            bean.height = this.BEAN_HEIGHT;
            bean.speedY = 0;
            bean.value = 200;
            this.beansGreen.push(bean);
        }
    },
    
    createYellowBeanSprites: function() {
        var bean;
        for (var i = 0; i < this.beanYellowData.length; i++) {
            bean = new Sprite("yellowBean", new SpriteSheetArtist(this.hazard, this.beanYellowCell), [this.dropBehaviour, this.pointsBehaviour, this.dropBurstBehaviour]);
            bean.width = this.BEAN_WIDTH;
            bean.height = this.BEAN_HEIGHT;
            bean.speedY = 0;
            bean.value = 150;
            this.prepareDropBurst(bean);
            this.beansYellow.push(bean);
        }
    },
    
    createWhiteBeanSprites: function() {
        var bean;
        for (var i = 0; i < this.beanWhiteData.length; i++) {
            bean = new Sprite("whiteBean", new SpriteSheetArtist(this.hazard, this.beanWhiteCell));
            bean.width = this.BEAN_WIDTH;
            bean.height = this.BEAN_HEIGHT;
            bean.speedY = 0;
            this.determineWhiteBeanBehaviour(bean);
            this.beansWhite.push(bean);
        }
    },
    
    createBombSprites: function() { 
        var bomb;
        for (var i = 0; i < this.bombData.length; i++) {
            var bombType = Math.floor(Math.random() * 2);
            bomb = new Sprite("bomb", new SpriteSheetArtist(this.hazard, this.bombCell));
            if (bombType === 0) {
                bomb.behaviours = [this.dropBehaviour, this.leftRightBehaviour, this.bombCollideBehaviour, new CycleBehaviour(42, 500), this.bombExplosionBehaviour];
            } else {
                bomb.behaviours = [this.dropBehaviour, this.bombCollideBehaviour, new CycleBehaviour(42, 500), this.bombExplosionBehaviour];
            }
            bomb.width = this.BOMB_WIDTH;
            bomb.height = this.BOMB_HEIGHT;
            bomb.speedY = 0;
            bomb.speedX = 100;
            bomb.exploding = false;
            this.bombs.push(bomb);
        }
    },
    
    createRubySprites: function() {
        var ruby,
            SPARKLE_DURATION = 100,
            SPARKLE_INTERVAL = 500;
        for (var i = 0; i < this.rubyData.length; i++) {
            ruby = new Sprite("ruby", new SpriteSheetArtist(this.hazard, this.rubyCells), [this.dropBehaviour, new CycleBehaviour(SPARKLE_DURATION, SPARKLE_INTERVAL), this.rubyCollideBehaviour]);
            ruby.width = this.rubyCells[0].width; //The widest of all the cells
            ruby.height = this.RUBY_HEIGHT;
            ruby.speedY = this.RUBY_DROP_VELOCITY;
            this.rubies.push(ruby);
        }
    },
    
    createPlayerSprite: function() {
        this.player = new Sprite("player", new SpriteSheetArtist(this.hazard, this.playerCells));
        this.player.top = this.positionY;
        this.player.left = this.positionX;
        this.player.width = this.PLAYER_WIDTH;
        this.player.height = this.PLAYER_HEIGHT;
    },
    
    prepareDropBurst: function(bean) {
        var BEAN_DROP_DURATION = 1600,
            EASING_FACTOR = 1.1;
    
        bean.DROP_LENGTH = rainBeans.BEAN_DROP_VELOCITY * 2;
        bean.DROP_DURATION = BEAN_DROP_DURATION;
        bean.dropping = false;
        bean.dropTimer = new AnimationTimer(bean.DROP_DURATION, AnimationTimer.makeEaseOutEasingFunction(EASING_FACTOR));
        
        bean.drop = function() {
            if (this.dropping) {
                return;
            }
            this.dropping = true;
            this.startingSpeed = this.DROP_LENGTH;
            this.dropTimer.start(rainBeans.timeSystem.calculateGameTime());
        };
        
        bean.stopDropping = function() {
            this.dropTimer.stop();
            this.dropping = false;
        };
    },
    
    determineWhiteBeanBehaviour: function(bean) {
        //The white bean will mimic the behaviours of other beans
        //This code will determine which bean it mimics
        var beanToMimic = Math.floor((Math.random() * 4));
        bean.beanMimiked = beanToMimic;
        
        if (bean.beanMimiked === 0) { //Mimic Red Bean
            bean.behaviours = [this.dropBehaviour, this.pointsBehaviour];
            bean.preparedSpeedY = rainBeans.BEAN_DROP_VELOCITY;
            bean.value = 100;
        } else if (bean.beanMimiked === 1) {//Mimic Blue Bean
            bean.behaviours = [this.dropBehaviour, this.leftRightBehaviour, this.pointsBehaviour];
            bean.preparedSpeedY = rainBeans.BEAN_DROP_VELOCITY;
            bean.speedX = 100;
            bean.value = 150;
        } else if (bean.beanMimiked === 2) {//Mimic Green Bean
            bean.behaviours = [this.dropBehaviour, this.pointsBehaviour];
            bean.preparedSpeedY = rainBeans.BEAN_DROP_VELOCITY * 2.5;
            bean.value = 200;
        } else {//Mimic Yellow Bean
            bean.behaviours = [this.dropBehaviour, this.pointsBehaviour, this.dropBurstBehaviour];
            bean.value = 150;
            this.prepareDropBurst(bean);
        }
    },
    
    initializeSprites: function() {
        this.positionSprites(this.beansRed, this.beanRedData);
        this.positionSprites(this.beansBlue, this.beanBlueData);
        this.positionSprites(this.beansGreen, this.beanGreenData);
        this.positionSprites(this.beansYellow, this.beanYellowData);
        this.positionSprites(this.beansWhite, this.beanWhiteData);
        this.positionSprites(this.bombs, this.bombData);
        this.positionSprites(this.rubies, this.rubyData);
        this.positionSprites(this.player, this.playerData);
    },
    
    addBeansToAllBeansArray: function() {
        for (var i = 0; i < this.beansRed.length; i++) {
            this.sprites.push(this.beansRed[i]);
        }
        for (var i = 0; i < this.beansBlue.length; i++) {
            this.sprites.push(this.beansBlue[i]);
        }
        for (var i = 0; i < this.beansGreen.length; i++) {
            this.sprites.push(this.beansGreen[i]);
        }
        for (var i = 0; i < this.beansYellow.length; i++) {
            this.sprites.push(this.beansYellow[i]);
        }
        for (var i = 0; i < this.beansWhite.length; i++) {
            this.sprites.push(this.beansWhite[i]);
        }
        for (var i = 0; i < this.bombs.length; i++) {
            this.sprites.push(this.bombs[i]);
        }
        for (var i = 0; i < this.rubies.length; i++) {
            this.sprites.push(this.rubies[i]);
        }
        this.sprites.push(this.player);
    },
    
    positionSprites: function(sprites, spriteData) {
        var sprite;
        for (var i = 0; i < sprites.length; ++i) {
            sprite = sprites[i];
            sprite.top = spriteData[i].top;
            sprite.left = spriteData[i].left;
            if (sprite.type === "bomb") {
                sprite.levelSpawn = spriteData[i].levelSpawn;
            }
        }
    },
    
   animate: function(now) {
       now = rainBeans.timeSystem.calculateGameTime();
       if (rainBeans.paused) {
           setTimeout(function() {
                requestAnimationFrame(rainBeans.animate);
           }, rainBeans.PAUSED_CHECK_INTERVAL);
       } else {
           fps = rainBeans.calculateFps(now);
           rainBeans.lowFPSAlert(fps);
           rainBeans.draw(now);
           rainBeans.lastAnimationFrameTime = now;
           requestAnimationFrame(rainBeans.animate);
       }
       
   },
   
   lowFPSAlert: function(fps) { //Pause the game once if a low fps is detected
       if (!this.fpsWarning && this.theBeansMoveNow) {
           if (fps < 30) {
               this.togglePause();
               alert("Warning: The fps dropped considerably low. Perhaps you should close any unneeded applications to optimize speed.");
               this.fpsWarning = true;
           }
       }
   },
   
   calculateFps: function(now) {
       var fps = 1/(now - this.lastAnimationFrameTime) * 1000 * this.timeRate;
       if(now - this.lastFpsUpdateTime > 1000)
       {
           this.lastFpsUpdateTime = now;
           this.fpsElement.innerHTML = fps.toFixed(0) + ' fps';
       }
       return fps;
   },
    
    //Set up the images for the background and the sprites
    initializeImages: function() {
        this.background.src = 'images/background.png';
        //this.player.src = "images/player.png";
        this.hazard.src = "images/object.png";
        this.loadingTitleElement.style.display = "block";
        
        this.background.onload = function(e) {
            rainBeans.backgroundLoaded();
        };
    },
    
    backgroundLoaded: function() {
      var LOADING_SCREEN_DURATION = 2000;
      setTimeout(function(e){
          rainBeans.loadingTitleElement.style.display = "none";
          rainBeans.startGame();
          rainBeans.gameStarted = true;
      }, LOADING_SCREEN_DURATION);
    },
    
    startGame: function() {
        this.revealStartupToast();
        this.timeSystem.start();
        //this.setTimeRate(0.5);
        window.requestAnimationFrame(rainBeans.animate);
    },
    
    setTimeRate: function(rate) {
        this.timeRate = rate;
        this.timeSystem.setTransducer(function(now) {
            return now * rainBeans.timeRate;
        });
    },
    
    revealStartupToast: function() {
        var INITIAL_DELAY = 1500,
            INITIAL_DURATION = 3000;
        setTimeout(function() {
            rainBeans.revealToast("Catch those beans!", INITIAL_DURATION);
        }, INITIAL_DELAY);
    },
    
    revealToast: function(text, duration) {
        duration = duration || 1000;
        this.startToastTransition(text, duration);
        setTimeout(function(e) {
            rainBeans.hideToast();
        }, duration);
    },
    
    startToastTransition: function(text, duration) {
        this.toastElement.innerHTML = text;
        this.fadeInElements(this.toastElement);
    },
    
    hideToast: function() {
        var TRANSITION_DURATION = 500;
        this.fadeOutElements(this.toastElement, TRANSITION_DURATION);
    },
    
    fadeInElements: function() {
        var args = arguments;
        for (var i = 0; i < args.length; ++i) {
            args[i].style.display = "block";
        }
        setTimeout(function(e) {
            for (var i = 0; i < args.length; ++i) {
                args[i].style.opacity = 1;
            }
        }, 50);
    },
    
    fadeOutElements: function() {
        var args = arguments,
            fadeDuration = args[args.length - 1];
        for (var i = 0; i < args.length - 1; ++i) {
            args[i].style.opacity = 0;
        }
        setTimeout(function(e) {
           for (var i = 0; i < args.length - 1; ++i) {
                args[i].style.display = "none";
            }
        }, fadeDuration);
    },
    
    //Draw the images
    draw: function(now) {
        rainBeans.setOffset(now);
        this.drawBackground();
        //this.drawPlayer();
        this.updateSprites(now);
        this.drawSprites();
        this.updateScore();
        this.showFinalScore();
    },
    
    drawBackground: function() {
        this.context.drawImage(this.background, 0, 0);
    },
    
    drawPlayer: function() {
        if (!this.gameOver) {
            //this.context.drawImage(this.player, this.positionX, this.positionY);
        }
    },
    
    updateSprites: function(now) {
        var sprite;
        for (var i = 0; i < this.sprites.length; ++i) {
            sprite = this.sprites[i];
            if (sprite.type === "blueBean" && rainBeans.currentPorgress >= 2) {
                sprite.speedY = rainBeans.BEAN_DROP_VELOCITY;
            }
            if ((sprite.type === "greenBean" && rainBeans.currentPorgress >= 3)) {
                sprite.speedY = rainBeans.BEAN_DROP_VELOCITY * 2.5;
            }
            if ((sprite.type === "yellowBean" && rainBeans.currentPorgress >= 3)) {
                sprite.drop();
            }
            if ((sprite.type === "whiteBean" && rainBeans.currentPorgress >= 3)) {
                if (sprite.beanMimiked === 3) {
                    sprite.drop();
                } else {
                    sprite.speedY = sprite.preparedSpeedY;
                }
            }
            if (sprite.type === "bomb") {
                this.spawnBomb(sprite);
            }
            sprite.update(now, this.fps, this.context, this.lastAnimationFrameTime);
        }
    },
    
    spawnBomb: function(bomb) {
        if (bomb.levelSpawn <= rainBeans.currentPorgress) {
            bomb.speedY = this.BOMB_DROP_VELCOITY;
        }
    },
    
    drawSprites: function() {
        var sprite;
        for (var i = 0; i < this.sprites.length; ++i) {
            sprite = this.sprites[i];
            sprite.draw(this.context);
        }
    },
    
    updateScore: function() {
        this.scoreElement.innerHTML = this.globalScore;
        this.checkProgression();
    },
    
    showFinalScore: function() {
        if (this.gameOver) {
            this.gameOverElement.innerHTML = "Game Over<br>Final Score: " + this.globalScore;
            this.gameOverElement.style.display = "block";
        }
    },
    
    checkProgression: function() {
        //Checks if your score is above a certain amount. If so, new beans are set to spawn
        var TO_LEVEL_TWO = 1000,
            TO_LEVEL_THREE = 2000;
        if (this.globalScore >= TO_LEVEL_TWO && this.globalScore < TO_LEVEL_THREE) {
            this.currentPorgress = this.LEVEL_TWO;
        } else if (this.globalScore >= TO_LEVEL_THREE) {
            this.currentPorgress = this.LEVEL_THREE;
        }
    },
    
    //Player movements
    goLeft: function() {
        this.theBeansMoveNow = true,
        this.isMoving = true;
        this.direction = this.LEFT;
        this.player.artist.cellIndex = 1;
    },
    
    goRight: function() {
        this.theBeansMoveNow = true,
        this.isMoving = true;
        this.direction = this.RIGHT;
        this.player.artist.cellIndex = 0;
    },
    
    setOffset: function(now) {
        var withinScreen = this.isPlayerWithinScreen();
        if (this.isMoving && withinScreen) {
            //this.player.speedX = this.PLAYER_VELOCITY * this.direction;
            this.positionX += (this.PLAYER_VELOCITY * this.direction) * (now - this.lastAnimationFrameTime) / 1000;
            this.player.left = this.positionX;
        }
    },
    
    togglePause: function() {
        //var now = +new Date();
        var now = this.timeSystem.calculateGameTime();
        this.paused = !this.paused;
        if (this.paused) {
            this.pauseStartTime = now;
        } else {
            this.lastAnimationFrameTime += (now - this.pauseStartTime);
        }
    },
    
    isPlayerWithinScreen: function() {
        if (this.positionX < 0 && this.direction === this.LEFT) {
            return false;
        }
        if (this.positionX > this.CANVAS_WIDTH - this.PLAYER_WIDTH && this.direction === this.RIGHT) {
            return false;
        }
        return true;
    },
    
    stopMoving: function() {
        this.isMoving = false;
    }
};

window.addEventListener("keydown", function(e) {
    var key = e.keyCode;
    if (key === rainBeans.KEY_A || key === rainBeans.KEY_LEFT) {
        rainBeans.goLeft();
    } else if (key === rainBeans.KEY_D || key === rainBeans.KEY_RIGHT) {
        rainBeans.goRight();
    } else if (key === rainBeans.KEY_P) {
        if (!rainBeans.gameOver) {
            rainBeans.togglePause();
        }
    }
});

window.addEventListener("keyup", function(e) {
    rainBeans.stopMoving();
});

window.addEventListener("blur", function(e) {
    rainBeans.windowFocused = false;
    if (!rainBeans.paused && !rainBeans.gameOver) {
        rainBeans.togglePause();
    }
});

window.addEventListener("focus", function(e) {
    var DIGIT_DISPLAY_DURATION = 500;
    
    rainBeans.windowFocused = true;
    rainBeans.countDownInProgress = true;
    if (rainBeans.paused && !rainBeans.gameOver) {
        if (rainBeans.windowFocused && rainBeans.countDownInProgress) {
            rainBeans.revealToast("3", DIGIT_DISPLAY_DURATION);
        }
        setTimeout(function(e) {
            if (rainBeans.windowFocused && rainBeans.countDownInProgress) {
                rainBeans.revealToast("2", DIGIT_DISPLAY_DURATION);
            }
            setTimeout(function(e) {
                if (rainBeans.windowFocused && rainBeans.countDownInProgress) {
                    rainBeans.revealToast("1", DIGIT_DISPLAY_DURATION);
                }
                setTimeout(function(e) {
                    if (rainBeans.windowFocused && rainBeans.countDownInProgress) {
                        rainBeans.togglePause();
                        rainBeans.revealToast("Back in the game!", 3000);
                    }
                    rainBeans.countDownInProgress = false;
                }, DIGIT_DISPLAY_DURATION);
            }, DIGIT_DISPLAY_DURATION);
        }, DIGIT_DISPLAY_DURATION);
    }
});

var rainBeans = new rainingBeans();
rainBeans.initializeImages();
rainBeans.createSprites();