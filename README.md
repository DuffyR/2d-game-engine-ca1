## It's Raining Beans. ##

The goal of Raining Beans is simple: Beans are falling, you catch them.

Bean Types:

* Red - The simplest

* Blue - Moves side to side

* Yellow - Small bursts of movement

* Green - Drops down so fast

* White - Anything goes.

* Bomb - Not a bean. Don't catch